# Data Trend Forecaster

## Overview

The **Data Trend Forecaster** is an advanced data analytics platform designed to integrate diverse data sources like social media trends, customer feedback, sales data, and influencer insights. By leveraging natural language processing (NLP), sentiment analysis, and machine learning algorithms, this platform predicts future fashion trends and provides actionable insights for stakeholders in the fashion industry.

## Features

- Data Integration: Consolidates data from social media, customer feedback, sales, and influencers.
- Sentiment Analysis: Utilizes NLP models to analyze sentiments in text data.
- Sales Forecasting: Implements time series forecasting models to predict future sales trends.
- Customer Segmentation: Segments customers based on feedback using clustering algorithms.
- Actionable Insights: Provides detailed insights and recommendations based on analyzed data.

## Installation

### Prerequisites
   ##Datasets 
   datasets such as women dresses review,sales,social media,products are taken from kaggle as an sample

Ensure you have Python installed on your system. Additionally, you will need the following libraries:
- streamlit
- pandas
- re
- altair
- nltk
- textblob
- prophet
- sklearn
- wordcloud
- matplotlib
- seaborn

You can install these dependencies using the following command:

```bash
pip install streamlit pandas altair nltk textblob prophet scikit-learn wordcloud matplotlib seaborn
Running the Streamlit App
Navigate to the project directory: cd data_trend_forecaster
Run the Streamlit app: streamlit run app.py
