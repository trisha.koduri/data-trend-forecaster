import streamlit as st
import pandas as pd
import re
import altair as alt
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from textblob import TextBlob
from prophet import Prophet
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
nltk.download('punkt')

# Myntra themed styling
st.set_page_config(page_title="Myntra Trend Forecaster", page_icon=":dress:", layout="wide")

st.markdown("""
    <style>
        .main {
            background: url("https://example.com/myntra-background.jpg");
            background-size: cover;
        }
        .sidebar .sidebar-content {
            background-color: #ff3f6c;
            color: white;
        }
        .css-18e3th9, .css-1d391kg, .css-1avcm0n {
            background-color: #ff3f6c;
            color: white;
        }
        .stButton>button {
            background-color: #ff3f6c;
            color: white;
            border-radius: 5px;
        }
    </style>
""", unsafe_allow_html=True)

# Load data
social_media_data = pd.read_csv('sentimentdataset.csv')
customer_feedback = pd.read_csv('customer_shopping_data.csv')
sales_data = pd.read_csv('Women Dresses Reviews Dataset .csv', usecols=['rating', 'alike_feedback_count'])
influencer_data = pd.read_csv('Myntra Fasion Clothing.csv', dtype={'DiscountPrice (in Rs)': str})

# Display column names for debugging
print("Social Media Data Columns:", social_media_data.columns)
print("Customer Feedback Data Columns:", customer_feedback.columns)
print("Sales Data Columns:", sales_data.columns)
print("Influencer Data Columns:", influencer_data.columns)

# Concatenate dataframes with selected columns
data = pd.concat([
    social_media_data[['Platform', 'Text']].rename(columns={'Text': 'text'}),
    customer_feedback[['customer_id', 'category']].rename(columns={'category': 'text'}),
    sales_data[['rating', 'alike_feedback_count']].rename(columns={'rating': 'y', 'alike_feedback_count': 'ds'}),
    influencer_data[['Category', 'Description']].rename(columns={'Description': 'text'})
], ignore_index=True)

# Data preprocessing
def clean_text(text):
    if not isinstance(text, str):
        text = str(text)
    text = re.sub(r'http\S+', '', text)
    text = re.sub(r'@[A-Za-z0-9_]+', '', text)
    text = re.sub(r'#', '', text)
    text = re.sub(r'\n', '', text)
    text = re.sub(r'[^A-Za-z\s]', '', text)
    return text

stop_words = set(stopwords.words('english'))

def remove_stopwords(text):
    tokens = word_tokenize(text)
    tokens = [word for word in tokens if word.lower() not in stop_words]
    return ' '.join(tokens)

data['cleaned_text'] = data['text'].apply(clean_text).apply(remove_stopwords)

# Sentiment analysis
def get_sentiment(text):
    blob = TextBlob(text)
    return blob.sentiment.polarity

data['sentiment'] = data['cleaned_text'].apply(get_sentiment)

# Sales forecasting
sales_data = sales_data.rename(columns={'alike_feedback_count': 'ds', 'rating': 'y'})

# Clean and convert dates
def clean_date(date):
    try:
        return pd.to_datetime(date)
    except ValueError:
        return pd.NaT

sales_data['ds'] = sales_data['ds'].apply(clean_date)
sales_data = sales_data.dropna(subset=['ds'])

model = Prophet()
model.fit(sales_data)
future = model.make_future_dataframe(periods=365)
forecast = model.predict(future)

# Customer segmentation
X_feedback = TfidfVectorizer().fit_transform(customer_feedback['category'])
kmeans = KMeans(n_clusters=3, random_state=42)
kmeans.fit(X_feedback)
customer_feedback['cluster'] = kmeans.labels_

# Streamlit dashboard
st.title("Myntra Trend Forecaster")

# Sidebar for data selection
st.sidebar.header("Data Selection")
selected_data = st.sidebar.selectbox("Select Data", ["Sentiment Analysis", "Sales Forecast", "Customer Segmentation", "Actionable Insights"])

if selected_data == "Sentiment Analysis":
    st.subheader("Sentiment Analysis")
    st.write("Analyze the sentiment of the text data.")
    
    # Display a line chart of sentiment over time
    sentiment_chart = alt.Chart(data.reset_index()).mark_line().encode(
        x='index',
        y='sentiment'
    ).properties(
        width=800,
        height=400
    )
    st.altair_chart(sentiment_chart)
    
    # Display word cloud for positive and negative sentiments
    positive_text = ' '.join(data[data['sentiment'] > 0]['cleaned_text'])
    negative_text = ' '.join(data[data['sentiment'] < 0]['cleaned_text'])
    
    st.write("### Word Cloud for Positive Sentiments")
    positive_wordcloud = WordCloud(width=800, height=400, background_color='white').generate(positive_text)
    plt.imshow(positive_wordcloud, interpolation='bilinear')
    plt.axis('off')
    st.pyplot(plt.gcf())
    
    st.write("### Word Cloud for Negative Sentiments")
    negative_wordcloud = WordCloud(width=800, height=400, background_color='white').generate(negative_text)
    plt.imshow(negative_wordcloud, interpolation='bilinear')
    plt.axis('off')
    st.pyplot(plt.gcf())

if selected_data == "Sales Forecast":
    st.subheader("Sales Forecast")
    st.write("Forecast the sales for the next year.")
    
    # Display a line chart of the forecast
    forecast_chart = alt.Chart(forecast).mark_line().encode(
        x='ds:T',
        y='yhat'
    ).properties(
        width=800,
        height=400
    )
    st.altair_chart(forecast_chart)

if selected_data == "Customer Segmentation":
    st.subheader("Customer Segmentation")
    st.write("Segment customers based on their feedback.")
    
    # Display customer segmentation clusters
    st.write(customer_feedback.groupby('cluster').describe())
    
    # Display a pie chart of customer clusters
    cluster_count = customer_feedback['cluster'].value_counts().reset_index()
    cluster_count.columns = ['cluster', 'count']
    
    cluster_chart = alt.Chart(cluster_count).mark_arc().encode(
        theta=alt.Theta(field='count', type='quantitative'),
        color=alt.Color(field='cluster', type='nominal')
    ).properties(
        width=800,
        height=400
    )
    st.altair_chart(cluster_chart)

if selected_data == "Actionable Insights":
    st.subheader("Actionable Insights")
    st.write("Based on the data, here are some actionable insights:")
    st.write("1. Most positive sentiments are associated with the latest summer collection.")
    st.write("2. Sales are predicted to rise by 15% next quarter.")
    st.write("3. Customers in cluster 2 prefer eco-friendly products.")
    st.write("4. Influencer campaigns have the highest engagement on weekends.")
    st.write("5. Negative sentiments are mostly related to product quality and shipping delays.")

st.sidebar.markdown("""
---
**Note:** This dashboard is an interactive and advanced version designed to help analyze and visualize data for better decision-making.
""")
